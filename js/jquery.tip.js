jQuery.fn.tip = function(texto,id)
{
	$(this).each(function(){
		elem = $(this);
		var ref = $('#'+id).text(texto);
		elem.data("ref",ref)
		elem.mouseenter(function(e){
			var elem = $(this);
			var ref = elem.data("ref");
			coord_X = e.pageX+5;
			coord_Y = e.pageY+5;
			ref.css({'display':'block','top':coord_Y,'left':coord_X,'border':'solid 1px #d3d3d3',
			'padding':'10px','background':'#f3f4f6','margin':'0','z-index':'1000'});			
		});
		elem.mouseleave(function(e){
			var elem = $(this);
			var ref = elem.data("ref");
			ref.css({'display':'none'});			
		});
	});
	return this;
}
