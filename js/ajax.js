/* 	fichero: ajax.js 

	Autor: William parras
	
	Descripcion: Este codigo es una adaptación del codigo extraido del libro "Ajax in Action", escrito por Dave Crane,
	Eric Pascarello y Darren James y publicado por la editorial Manning que tambien fue modificado por Javier Eguíluz Pérez
	en su libro AJAX.
	El codigo utiliza notación de puntos, parte de la "Java Script Object Notation" (JSON).
	
	Uso: para disponer del codigo y utilizarlo debe incluirlo en un documento HTML o xHTMl y asegurarse que su navegador soporte
	Javascript. Navegadores como IE 7 y sus predecesores no soportan algunas de las funciones implementadas en este fichero.
*/

/* Source Code ---- Codigo Fuente */

// Creación el objeto Ajax
var ajax = new Object(); // instanciación como objeto genérico

// Definicion de todos los posibles estados de la peticion HTTP
ajax.READY_STATE_UNINITIALIZED=0; // No se ha iniciado la petición
ajax.READY_STATE_LOADING=1; // Cargando, aun no se llama al metodo open
ajax.READY_STATE_LOADED=2; // Cargado, se llamo al metodo open
ajax.READY_STATE_INTERACTIVE=3; // Se recibieron algunos datos pero no todos
ajax.READY_STATE_COMPLETE=4; // Han sido recibidos todos los datos del servidor 

// Funcion constructor

/*	Descripción de Parametros:

		url: Referencia a la URL encargada de procesar la respuesta del servidor.
		funcion: Funcion que se ejecutara si la petición AJAX resulta ser exitosa.
		metodo: Tipo de metodo a utilizar en la petición, puede tomar los valores "POST" o "GET".
		parametros: Hace referencia a todos aquellos parametros que seran enviados al servidor
				   Es posible utilizar una funcion que genere una cadena de texto que contenga 
				   los datos a ser enviados.
		contentType: Es la cabecera content-type de la petición, si no se define correctamente 
				   ningun dato sera enviado al servidor. Usualmente se utiliza la cabecera
				   "application/x-www-form-urlencoded".
		funcionError: Este es un parametro opcional, Es una referencia a una funcion externa 
				   que muestra información en caso de error. Para especificar una Función
				   dene definirse primero y luego pasarla como parametro sin parentesis. En
				   caso que no se indique una función se usara una por defecto que fue definida 
				   dentro del objeto ajax.
*/

ajax.START = function(url, id, metodo, parametros, contentType,funcionError) {
// this es hace referencia al objeto que invoca la función ( this = ajax.START  )
	// Asignacion de parametros a variables del objeto
	this.url = url; // ajax.START.url = url 
	this.req_HTTP = null; // ajax.START.req_HTTP = url ---> variable que almacenará el objeto XMLHttpRequest
	this.idloader = id; // ajax.START.onload = funcion ---> funcion EXITO 
	this.onerror = (funcionError) ? funcionError : this.defaultError; // ajax.START.onerror = funcionError : ajax.START.defaultError 
	this.cargaContenidoXML(url, metodo, parametros, contentType); // llamada a la funcion ajax.START.cargaContenidoXML
} // Fin de función constructor


/* Definicion de metodos para el objeto ajax por medio de la propiedad prototype
-----------------------------------------------------------------------------------------------
*/

ajax.START.prototype = {
// Referencia a la función -------> ajax.START.cargaContenidoXML NOTA: esta función se invoca desde el constructor
	cargaContenidoXML: function(url, metodo, parametros, contentType) {
	// 1. Obtiene la instancia del objeto XMLHttpRequest segun el navegador  
		if(window.XMLHttpRequest) { // Navegadores que cumplen con el estandar
			this.req_HTTP = new XMLHttpRequest();
		}
		else if(window.ActiveXObject) {
			this.req_HTTP = new ActiveXObject("Microsoft.XMLHTTP"); // Navegadores antiguos
		}
		// Verifica que se haya instanciado el objeto y lanza un bloque try
		if(this.req_HTTP) {
			try {
				var loader = this; // almacena el objeto invocador ----> ajax.START para usarlo como parametro
							    // en una llamada a una funcion por medios del metodo call
				this.req_HTTP.onreadystatechange = function() { // Al detectar un cambio en el estado de respuesta
					loader.onReadyState.call(loader); // llama a la funcion que muestra los datos
					// Esta función se llama cada vez que cambia el estado de la petición
				}
				this.req_HTTP.open(metodo, url, true);// Invoca el metodo open para preparar la petición
				if(contentType) {
					this.req_HTTP.setRequestHeader("Content-Type", contentType);// Prepara las cabeceras de la petición 
				}
				this.req_HTTP.send(parametros); // Envia los parametros al servidor por medio del metodo send
			} catch(err) {
				this.onerror.call(this); // lanza una Excepcion en caso que falle el bloque try
			}
		}
	},
	// Funcion que mostrará los datos
	onReadyState: function() { 
		var req_HTTP = this.req_HTTP; // almacena el objeto XMLHttpRequest
		var ready = req_HTTP.readyState; // variable que representa que se recibio la respuesta completa
		// Debido a que esta función se llama recursivamente en cada cambio de estado en la petición 
		// en cada llamada verifica el estado de la petición, si el estado es diferende de READY_STATE_COMPLETE no se
		// ejecuta el bloque
		if(ready == ajax.READY_STATE_COMPLETE) {
			var httpStatus = req_HTTP.status;// Se obtiene el status te la peticion
			// 200 = EXITO, 404 = No se pudo conectar al servidor 
			if(httpStatus == 200 || httpStatus == 0) {
				document.getElementById(this.idloader).innerHTML=this.req_HTTP.responseText.tratarResponseText();
			}
			else {
				this.onerror.call(this); // Funcion de Error
			}
		}
	},
	// Funcion Error por defecto, Mustra algunos datos de la petición
	defaultError: function() {
		alert("Se ha producido un error al obtener los datos"+"\n\nreadyState:" + this.req_HTTP.readyState + "\nstatus: " + this.req_HTTP.status + "\nheaders: " + this.req_HTTP.getAllResponseHeaders());
	}
}

//prototipo que sirve para tratar la respuesta:
String.prototype.tratarResponseText=function(){
    var pat=/<script[^>]*>([\S\s]*?)<\/script[^>]*>/ig;
    var pat2=/\b\s+src=[^>\s]+\b/g;
    var elementos = this.match(pat) || [];
    for(i=0;i<elementos.length;i++) {
        var nuevoScript = document.createElement('script');
        nuevoScript.type = 'text/javascript';
        var tienesrc=elementos[i].match(pat2) || [];
        if(tienesrc.length){
            nuevoScript.src=tienesrc[0].split("'").join('').split('"').join('').split('src=').join('').split(' ').join('');
        }else{
            var elemento = elementos[i].replace(pat,'$1');
            nuevoScript.text = elemento;
        }
        document.getElementsByTagName('body')[0].appendChild(nuevoScript);
    }
    return this.replace(pat,'');
}
