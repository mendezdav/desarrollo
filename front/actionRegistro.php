<?php
	require_once '../phpclasses/conexion.php';
	conectar();
	if(isset($_POST['nombre'])&&isset($_POST['apellido'])&&isset($_POST['usuario'])&&isset($_POST['correo'])&&isset($_POST['pass1'])&&isset($_POST['pass2']))
	{
		
		// Asignacion de todas las variables 
		$nombre = $_POST['nombre'];
		$apellido = $_POST['apellido'];
		$usuario = $_POST['usuario'];
		$correo = $_POST['correo'];
		$pass = $_POST['pass1'];
		$pass2 = $_POST['pass2'];
		
		// Banderas
		
		$flag1 = false;
		$flag2 = false;
		
		// verifica que todos los campos tengan algo escrito
		if(strlen($nombre) < 1 || strlen($apellido) < 1 || strlen($usuario) < 1 || strlen($correo) < 1 || strlen($pass) < 6 || strlen($pass2) < 6 )
		{
			// redirecciona a registro con codigo de error = 0
			header("location: ../registro.php?error=\"0\"");	
		}
		else 
		{
			// ya se comprobo que los campos estuvieran llenos, se procede a comprobar el correo electronico y luego si las 
			// claves coinciden
			
			// Expresion regular para coprobar el correo
			$pattern = "/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/";
			if(!preg_match($pattern, $correo))
			{
				// redirecciona a registro con codigo de error = 1
				header("location: ../registro.php?error=\"1\"");
			}
			else 
			{
				$flag1 = true;
			}
			
			// compara si las contraseñas son iguales
			if($pass != $pass2)
			{
				// redirecciona a registro con codigo de error = 2
				header("location: ../registro.php?error='2'");
			}
			else
			{
				$flag2 = true;
			}				
						
			// verifica estado de banderas, esto puede no ser necesario pero ayuda a asegurar que 
			// todas las operaciones se hayan realizado de manera correcta y no haya fallo en la peticion			
			if($flag1 && $flag2)
			{
				// Aqui realiza la consulta
				$pass = md5($pass);
				$query = "insert into usuario(usuario,password,idTipoUsuario) values('{$usuario}','{$pass}',2)";
				$result = mysql_query($query)or die("Erro al insertar usuario");
				$user = "select idUsuario from usuario where usuario='{$usuario}'";
				$result2 = mysql_query($user)or die("Error al obtener ID");
				while($row = mysql_fetch_assoc($result2))
				{
					$id = $row['idUsuario'];				
				}
				$mailSQL = "insert into correo(idUsuario,correo,idLocalizacion) values('{$id}','{$correo}',0)";
				$execmailSQL = mysql_query($mailSQL)or die("Error al insertar correo");
				$curriSQL = "insert into curriculum(idUsuario,nombre1,apellido1,fechaNacimiento,idPais,idTratamiento,idSexo,NIT,idUnidadEstatura,idUnidadPeso,idPaisActual) values('{$id}','{$nombre}','{$apellido}','1000-01-01 00:00:00',1,0,0,0,0,0,0)";
				$execurriSQL = mysql_query($curriSQL)or die("Error al insertar nombre y apellido");
				header("location: ../registro.php?status=\"1\"");				
			}
			else
			{
				// redirecciona a registro con codigo de error = 3
				header("location: ../registro.php?error=\"3\"");
			}
			
		}					
		
	}
	else 
	{
		// si las variables no estan definidas se redireccion al usuario hacia la pagina de inicio
		// esto evita que se acceda de manera inapropiada a la pagina 
		header("location: ../index.php");		
	}
?>