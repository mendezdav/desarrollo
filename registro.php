<?php	
	$css = "css/estilo.css"; 
	$tituloPagina="Registro";
	
	include("plantilla/header.php");
?>
<script type="text/javascript">
// Espera la carga del documento
	$('document').ready(function()
			{	
				$('#form1').each (function(){
		  			this.reset();
				});
			/* Banderas de estado, una bandera por cada campo
			 * Indican el estado de los formularios en todo momento
			 * false indica que los datos no son correctos
			 * true indica que los datos son correctos
			 */
				var flag1 = false;
				var flag2 = false;
				var flag3 = false;
				var flag4 = false;
				var flag5 = false;
				var flag6 = false;
				
			/* Funcion que verifica el estado de las banderas y segun 
			 * la respuesta habilita o deshabilita el boton de envio 
			 */
				boton = function()
				{	
					var btn = document.getElementById('env');
					// comparacion 
					if(flag1 && flag2 && flag3 && flag4 && flag5 && flag6 )
					{
						btn.disabled = false;
					}
					else
					{
						btn.disabled = true;	
					}	
				}
				
				// Deshabilitando boton para evitar envio de datos erroneos
				document.getElementById('env').disabled = true;
				
				// Asigna evento de pulso en tecla para campo usuario
				$('#usuario').keyup(function(e){										
					if(document.getElementById('usuario').value.length>4)
					{
						// cabecera de la peticion
						var content = 'application/x-www-form-urlencoded';
						// destino de los datos
						var url = 'front/actionValidarUsuario.php';
						// parametros a ser enviados al servidor
						var param = 'user='+encodeURIComponent(document.getElementById('usuario').value);
						// creacion de la peticion AJAX
						var peticion = new ajax.START(url,'us','POST',param,content);
						// verifica la respuesta devuelta por el servidor
						if($('img','#us').attr('alt') == 'ok')
						{
							// si la respuesta es exitosa activa una bandera
							flag1 = true; 
						}
						else
						{
							// si la respuesta falla desactiva la bandera
							flag2 = false;							
						}	
						// funcion activa boton
						boton();								
					}
					else
					{
						document.getElementById('us').innerHTML = '<img src="img/error.png" alt="error" />';	
						flag2 = false;
					}					
				});
				
				// Asigna evento de pulso en tecla para campo pass1
				$('#pass1').keyup(function(e){
					// almacena todos los campos involucrados en la operacion
					var campo = document.getElementById('pass1');
					var mensaje = document.getElementById('ps');
					var pass2 = document.getElementById('pass2').value;
					var mensaje = document.getElementById('ps');					
					// compara si las claves 1 y dos con iguales y pone a true o false la respectiva bandera
					// ademas muestra un respectivo mensaje al usuario
					if(campo.value != pass2)
					{
						mensaje.innerHTML = '<img src="img/error.png" alt="error" /> claves no coinciden';		
						flag3 = false;			
					}								
					else
					{
						mensaje.innerHTML = '<img src="img/exito.png" alt="exito" /> claves coinciden';
						flag3 = true;
					}  
					// luego verifica el largo de la contraseña y pone los valores respectivos a las banderas
					if(campo.value.length<6)
					{
						mensaje.innerHTML = '<img src="img/error.png" alt="error" />';		
						flag2 = false;				
					}
					else if(campo.value.length>=6 && campo.value.length<10)
					{
						mensaje.innerHTML = '<img src="img/exito.png" alt="exito" /> nivel de seguridad bajo ';
						flag2 = true;					
					}
					else if(campo.value.length>=10 && campo.value.length<15)
					{
						mensaje.innerHTML = '<img src="img/exito.png" alt="exito" /> nivel de seguridad medio ';
						flag2 = true;
					}
					else
					{
						mensaje.innerHTML = '<img src="img/exito.png" alt="exito" /> nivel de seguridad alto ';
						flag2 = true;
					}
					// actualiza estado del boton
					boton();
				});
				
				// Asigna evento de pulso en tecla para campo pass2
				$('#pass2').keyup(function(e){
					// almacena todos los campos involucrados en la operacion
					var pass1 = document.getElementById('pass1').value;
					var pass2 = document.getElementById('pass2').value;
					var mensaje = document.getElementById('ps');
					// no se necesita comparar la longitud para la pass repetida
					// solo se comprueban si son iguales y se ponen los valores respectivos a las respectivas banderas
					if(pass1 != pass2)
					{
						mensaje.innerHTML = '<img src="img/error.png" alt="error" /> claves no coinciden';		
						flag3 = false;			
					}								
					else
					{
						mensaje.innerHTML = '<img src="img/exito.png" alt="exito" /> claves coinciden';
						flag3 = true;
					}
					// actualiza el estado del boton
					boton();
				});
				
				// Asigna evento de pulso en tecla para campo nombre
				$('#nombre').keyup(function(e){
					// captura campo
					var nombre = document.getElementById('nombre').value;
					// verifica que haya algo en el campo 
					if(nombre.length > 3)
					{
						document.getElementById('no').innerHTML = '<img src="img/exito.png" alt="exito" />';
						flag4 = true;	
					}
					else
					{
						document.getElementById('no').innerHTML = '<img src="img/error.png" alt="error" />';
						flag4 = false;
					}
					// actualiza estado del boton
					boton();
				});
				
				// Asigna evento de pulso en tecla para campo apellido
				$('#apellido').keyup(function(e){
					// captura el campo
					var apellido = document.getElementById('apellido').value;
					// verifica que haya algo en el campo
					if(apellido.length > 3)
					{
						document.getElementById('ap').innerHTML = '<img src="img/exito.png" alt="exito" />';
						flag5 = true;	
					}
					else
					{
						document.getElementById('ap').innerHTML = '<img src="img/error.png" alt="error" />';
						flag5 = false;
					}
					// actualiza estado del boton
					boton();
				});
				
				// Asigna evento de pulso en tecla para campo correo
				$('#correo').keyup(function(e){
					// obtiene el campo
					var correo = document.getElementById('correo').value;
					// campo de alerta
					var com = document.getElementById('co');
					// compara con una Expresion regular para validar direccion de correo
					if(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(correo))
					{
						com.innerHTML = '<img src="img/exito.png" alt="exito" /> correo valido';
						flag6 = true;
					}
					else
					{
						com.innerHTML = '<img src="img/error.png" alt="error" /> correo no valido';
						flag6 = false;
					}					
					// actualiza estado del boton	
					boton();
				});
			});
</script>
<form method="post" action="front/actionRegistro.php" id="form1">
	<p>
		* Todos los campos son obligatorios
		<table>
			<tr>
				<td>
					Nombre:
				</td>
				<td>
					<input type="text" id="nombre" name="nombre" /><span id="no"></span>			
				</td>
			</tr>
			<tr>
				<td>
					Apellido:
				</td>
				<td>
					<input type="text" id="apellido" name="apellido" /><span id="ap"></span>				
				</td>
			</tr>
			<tr>
				<td>
					&nbsp;
				</td>
				<td>
					*minimo 5 caracteres				
				</td>			
			</tr>
			<tr>
				<td>
					Usuario:
				</td>
				<td>
					<input type="text" id="usuario" name="usuario" /><span id="us"></span>		
				</td>
			</tr>
			<tr>
				<td>				
					Correo:
				</td>
				<td>
					<input type="text" id="correo" name="correo" /><span id="co"><?php 
					if(isset($_GET['error']) && $_GET['error']==1){echo "correo no valido";}					
					?></span>				
				</td>
			</tr>
			<tr>
				<td>
					&nbsp;
				</td>
				<td>
					*minimo 6 caracteres				
				</td>			
			</tr>
			<tr>
				<td>
					Clave:
				</td>
				<td>
					<input type="password" id="pass1" name="pass1" /><span id="ps"><?php
					if(isset($_GET['error']) && $_GET['error']==2){echo "claves no coinciden";}
					?></span>				
				</td>
			</tr>
			<tr>
				<td>
					Repita su clave:
				</td>
				<td>
					<input type="password" id="pass2" name="pass2" />				
				</td>
			</tr>
			<tr>
			</tr>
		</table>
		<br />
		<input type="submit" value="Registrarme" id="env"/><span id="status"><?php 
		if(isset($_GET['status'])){echo "Te has Registrado correctamente!";}
		if(isset($_GET['error']) && $_GET['error']==0){echo "por favor rellena todos los campos";} 
		if(isset($_GET['error']) && $_GET['error']==3){echo "Rellene correctamente todos los campos";} 
		?></span>
	</p>
</form>
<?php	
	
	include("plantilla/footer.php");
?>
